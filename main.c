#include <stdio.h>
#include <stdlib.h>
int add(int, int);
int sub(int, int);
int mul(int, int);
int div(int, int);
int main() {
	int x, y;
	scanf("%d%d", &x, &y);
	printf("%d\n", add(x, y));
	printf("%d\n", sub(x, y));
	printf("%d\n", mul(x, y));
	printf("%d\n", div(x, y));
	return 0;
}
